from keboola.http_client import HttpClient
from requests import HTTPError
from typing import Dict, List

BASE_URL = "https://fakestoreapi.com/"

PRODUCTS_ENDPOINT = "products"
CARTS_ENDPOINT = "carts"


class FakeStoreClientException(Exception):
    pass


class FakeStoreClient(HttpClient):
    def __init__(self):
        super().__init__(BASE_URL)

    def get_products(self) -> List[Dict]:
        try:
            products = self.get(endpoint_path=PRODUCTS_ENDPOINT)
            return products
        except HTTPError as http_error:
            raise FakeStoreClientException(http_error)

    def get_carts(self):
        try:
            return self.get(endpoint_path=CARTS_ENDPOINT)
        except HTTPError as http_error:
            raise FakeStoreClientException(http_error)
