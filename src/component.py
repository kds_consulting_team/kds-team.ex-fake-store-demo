import logging

from keboola.component.dao import TableDefinition

from typing import Dict, List
from fakestore import FakeStoreClient, FakeStoreClientException
from csv import DictWriter
from custom_parser import MulitCsvJsonParser, FlattenJsonParser

from keboola.component.base import ComponentBase
from keboola.component.exceptions import UserException

KEY_ENDPOINT = "endpoint"
KEY_TABLE_NAME = "table_name"
KEY_TABLE_PREFIX = "table_prefix"

REQUIRED_PARAMETERS = [KEY_ENDPOINT]
REQUIRED_IMAGE_PARS = []


class Component(ComponentBase):

    def __init__(self):
        super().__init__()

    def run(self):
        self.validate_configuration_parameters(REQUIRED_PARAMETERS)
        self.validate_image_parameters(REQUIRED_IMAGE_PARS)
        params = self.configuration.parameters
        endpoint = params.get(KEY_ENDPOINT)

        client = FakeStoreClient()

        if endpoint == "Carts":
            logging.info("Fetching Cart data")
            self.fetch_and_write_carts(client)

        if endpoint == "Products":
            logging.info("Fetching Products data")
            self.fetch_and_write_products(client)

    def fetch_and_write_carts(self, client: FakeStoreClient):
        try:
            carts = client.get_carts()
        except FakeStoreClientException as ex:
            raise UserException(f"Failed to get cart data : f{ex}")
        json_parser = MulitCsvJsonParser()
        parsed_data = json_parser.parse_data(carts, "carts")
        for table_data_key in parsed_data:
            table_name = self.get_cart_table_name(table_data_key)
            table = self.create_out_table_definition(table_name)
            self.write_to_table(table, parsed_data[table_data_key])
            self.write_manifest(table)

    def get_cart_table_name(self, table_data_key: str) -> str:
        params = self.configuration.parameters
        table_prefix = params.get(KEY_TABLE_PREFIX)
        table_name = "".join([table_data_key, ".csv"])
        if table_prefix:
            table_name = "".join([table_prefix, "_", table_data_key, ".csv"])
        return table_name

    def fetch_and_write_products(self, client: FakeStoreClient):
        try:
            products = client.get_products()
        except FakeStoreClientException as ex:
            raise UserException(f"Failed to get products data : f{ex}")
        json_parser = FlattenJsonParser()
        parsed_data = json_parser.parse_data(products)

        table_name = self.get_product_table_name()
        table = self.create_out_table_definition(table_name)
        self.write_to_table(table, parsed_data)

    def get_product_table_name(self) -> str:
        params = self.configuration.parameters
        table_name = params.get(KEY_TABLE_NAME)
        if not table_name:
            table_name = "products.csv"
        return table_name

    @staticmethod
    def _infer_fieldnames_from_data(data: List[Dict]) -> List:
        keys = data[0].keys()
        return list(keys)

    def write_to_table(self, table: TableDefinition, data: List[Dict]):
        table.columns = self._infer_fieldnames_from_data(data)
        with open(table.full_path, 'w') as csv_file:
            writer = DictWriter(csv_file, fieldnames=table.columns)
            writer.writerows(data)
        self.write_manifest(table)


if __name__ == "__main__":
    try:
        comp = Component()
        comp.execute_action()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
